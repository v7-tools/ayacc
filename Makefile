# Makefile for ayacc
# Original author: Bell Labs
# Current author: calmsacibis995

UNAME := $(shell uname)

# Are we running Linux?
ifeq ($(UNAME), Linux)
CFLAGS=-s -O -std=c89
endif

# Are we running macOS?
ifeq ($(UNAME), Darwin)
CFLAGS=-s -O -Wno-error -std=c89
endif

head: ayacc 
ayacc: y1.o y2.o y3.o y4.o
	$(CC) $(CFLAGS) -o ayacc y?.o 
y1.o y2.o y3.o y4.o: dextern files
install:
	install -s ayacc $(DESTDIR)/usr/local/bin
	install -c yaccpar $(DESTDIR)/usr/local/lib

installman:
	install ayacc.1 $(DESTDIR)/usr/local/share/man/man1
clean:
	rm -rf *.o ayacc yacc.acts yacc.tmp y.tab.c
