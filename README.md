# ayacc
ayacc (Ancient Yacc) is the version of Yacc present in UNIX V7 and UNIX/32V. It has been mostly updated, aside from a few warnings.

## Building ayacc
* Requirements:
   * `gcc`
   * `make`
* Use `make` to build.
* Use `make install` (as root) or `sudo make install` (as non-root) to install ayacc to `/usr/local/bin`.
* The resulting files:
   * `ayacc` (Main binary)
   * `ayacc.1` (Manual page)
   * `yaccpar` (Parser file for ayacc)
   
## Submitting patches, bug fixes, updates to the code

Submit patches, bug fixes, and updates to the code by making pull requests.
